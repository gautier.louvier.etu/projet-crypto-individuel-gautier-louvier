import React, { Component } from 'react'

class Results extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }

  handleReset = () => {
    this.props.onReset()
}

  render() {
    return (
      <div className="container">
        <h2>Résultats</h2>
        <div className="row h-100">
          <div className="col-sm-12 my-auto">
            <div className="input-group w-25 mx-auto">
              {this.props.winner !== null && (
                <h3>Proposition gagnante : {this.props.winner}</h3>
              )}
            </div>
          </div>
        </div>
        <button className='btn btn-warning' onClick={this.handleReset}>Nouveau Vote</button>
      </div>
    )
  }
}

export default Results