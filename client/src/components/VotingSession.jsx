import React, { Component } from 'react'

class VotingSession extends Component {
    constructor(props) {
        super(props)
        this.state = {
          idProposal: "",
        }
    }

    handleProposalSelection = (event) => {
        this.setState({ idProposal: event.target.value})
    }

    handleRegisterVote = () => {
        const { idProposal } = this.state

        if (idProposal) {
            this.props.registerVote(idProposal)
            this.setState({ idProposal: "" })
        }
    }

    handleEndVotingSession = () => {
        this.props.onEndSession()
    }

    render() {
        return (
            <div className="container">
                <h2>Session de vote</h2>
                <div className="row h-100">
                    <div className="col-sm-12 my-auto">
                        <div className="input-group w-50 mx-auto">
                            <select className="form-control" value={this.state.idProposal} onChange={this.handleProposalSelection}>
                                <option value="">Sélectionnez une proposition</option>
                                {this.props.registeredProposals.map( (proposal, i) => (
                                        <option key={i} value={i}>{proposal}</option>
                                ))}
                            </select>
                            <button className="btn btn-outline-secondary" onClick={this.handleRegisterVote}>Voter</button>
                        </div>
                    </div>
                </div>
                <button className='btn btn-warning' onClick={this.handleEndVotingSession}>Terminer l'enregistrement des votes</button>
            </div>
        )
    }
}

export default VotingSession