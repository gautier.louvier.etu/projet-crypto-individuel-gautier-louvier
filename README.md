# Projet individuel - Contrat intelligent : Système de vote - Gautier Louvier

## Comment lancer l'application apres avoir clone le GitLab

```sh
$ cd ./truffle
$ npm install
```

Installer les pacquets pour l'app

```sh
$ cd ./client
$ npm install
```

Lancer Ganache () et le laisser ouvert dans un terminal

```sh
$ ganache
```

Si ganache n'est pas installer

```sh
$ npx install -g ganache
```

Migrer le contrat

```sh
#dans un terminal différent de celui ou est lancée Ganache
$ cd ./truffle
#deployer le contrat sur ganache
$ truffle migrate
```

Lancer le react dev server - dApp

```sh
$ cd client
$ npm start
```

## Extension navigateur

Veuillez installer l'extension MetaMask sur votre navigateur ce qui vous permettra de faire le lien avec Ganache
Créer vous un compte si besoin
Importer les clés privées générer par ganache dans le metamask 

## Configuration Extension

    Réseaux > Ajouter un réseau > Ajouter manuellement un réseau
      Nom du réseau:
        - localhost
      Nouvelle URL de RPC:
        - http://localhost:8545
      ID de chaîne:
        - 1337
      Symbole de la devise:
        - ETH
      URL de l’explorateur de blocs (Facultatif):
        - http://localhost:8545

## Vidéo démonstrative de la DApp

[Vidéo Démonstration Projet Individuel - YouTube](https://youtu.be/M6h6O4AdhFc)